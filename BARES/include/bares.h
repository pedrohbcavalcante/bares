#ifndef _STACKAR1_H_
#define _STACKAR1_H_

#include <iostream>
#include <stdexcept>
#include "doublearray.h"

template <class objeto>
class Stack{
public:

	//Classe pilha
	explicit Stack (int _size = 20);

	//Destrutor
	~Stack();

/*	Adiciona objeto na pilha
	@param X é o objeto a ser inserido*/

	void push(const objeto & x);

	/**
	* Retorna elemento mais recente da pilha
	* @return objeto mais recente
	**/

	const objeto & pop();

	/**
	* Retorna elemento do topo da pilha
	* @return elemento no topo
	**/

	const objeto & top() const;

	/**Testa se a pilha encontra-se vazia
	* @return true caso esteja vazia ou false caso não esteja vazia
	**/
	bool empty() const;

	/**
	* manipula a pilha para deixa-la vazia
	**/

	void clear();

private:
	int topo;
	int capacity;
	objeto *data;
};

class fila{
public:
	//Construtor
	explicit fila(int _size = 50);


	/**
	* remove o item mais recente da fila
	* @return o item mais recente
	**/
	char rm_fila();

	//Adiciona um caractere na fila

	void in_fila(const char & x);

	/**
	* retorna o item mais recente da fila
	* @return item mais recente
	**/

	const char & file_front() const;

	/**
	* Testa se a fila está vazia
	* @return true caso vazia ou false caso não vazia
	**/

	bool empty() const;

	//Manipula a fila para deixá-la vazia

	void clear();

private:
	int capacity;
	int front;
	int back;
	char *data;
};

template <typename Key, typename Info>
class LLSeq{
protected:
	struct NoLLSeq{
		Key id;
		Info info;
	};

	static const int SIZE = 50;
	int comprimento; 
	int capacity;
	NoLLSeq *mpt_Data;

	/**
	* Busca privada
	* @param recebe uma chave 
	* @return posição da chave
	**/
	int buscaPriv(Key _x) const;

public:
	//Construtor
	LLSeq(int _MaxSz = SIZE);

	//Destrutor

	virtual ~LLSeq();

	/**
	* Busca publica
	* @return true caso a chave esteja na lista ou false caso contrario
	**/
	bool buscaPub(Key _x, Info & _valor_rm) const;

	/**
	* remove item da lista
	* @return true se a remoção foi bem sucedida, false caso contrário
	**/
	bool rm_llseq( Key _x, Info &_valRemovido);

	/**
	* Insere item na lista
	* @return true se inserção foi feita false caso contrario
	**/
	bool in_llseq(Key _newId, Info _newInfo);

	//Imprime a lista
	void print() const;

};

/**
* Função responsável pela procedência de operadores
* @param operador1 primeiro operando
* @param operador2 segundo operando
* @return true caso operador1 tenha procedencia sobre operador 2 ou false caso contrário
**/


bool procedencia (char operando1, char operando2);

/**
* função que transforma de infixa para posfixa
* @param fila_infixa fica contento os caracteres
* @return fila posfixa
**/

fila infixa_posfixa(fila fila_infixa);

/**
* avalia a expressão posfica
* @param fila_posfixa
* @return resultado da avaliação
**/

int avaliacao_posfixa(fila fila_posfixa);

#include "bares.cpp"
#endif
