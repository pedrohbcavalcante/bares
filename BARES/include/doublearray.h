#ifndef _DOUBLE_ARRAY_H
#define _DOUBLE_ARRAY_H
#include <cassert> 
#include <new>

using std::bad_alloc;



template< typename T >
void
DoubleArray ( T * & A, int& size ) {
  T* auxVet;

  // 1) Create new doubled array.
  try { 
    auxVet = new T[ size * 2 ]; 
  }
  catch ( bad_alloc& e ) {
      std::cerr << "\n[DoubleArray()]:Error during doubling the array size!\n";
      throw e;
  }

  // 2) Copy elements from the old array to the newly allocated one,
  for ( int ct = 0; ct < size; ct++ ) {
    auxVet[ ct ] = A[ ct ];
  }

  // 3) Delete old array.
  delete [] A;

  // 4) Make the outside pointer points to the newly allocated area.
  A = auxVet;

  // 5) Double the variable that controls the array's size.
  size *= 2;
}

#endif

/* ------------------- [ End of the StackAr.cpp source ] ------------------- */
/* ========================================================================= */
