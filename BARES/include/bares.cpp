#include "bares.h"
#include <iostream>
using namespace std;
//
template <class objeto>
Stack<objeto>::Stack( int _size ) : topo( -1 ), capacity( _size ), data( NULL ){
	//std::cout << "Entrou em construtor stack" << endl;
    try {  
    	data = new objeto[ capacity ];  
    }
    catch ( std::bad_alloc& e ) {
        std::cerr << "  >>> [Stack<T>::Stack()] Dynamic memory allocation failed!\n";
        throw e;
    }
}
//
template <class objeto>
Stack<objeto>::~Stack(){
	//std::cout << "Entrou em destrutor stack" << endl;
    delete [] data;
}
//
template <class objeto>
void Stack<objeto>::push( const objeto & _x ){
	//std::cout << "Entrou em push stack" << endl;
    if( topo == capacity -1 ){
        DoubleArray( data, capacity );
    }
    data[ ++topo ] = _x;
}
//
template <class objeto>
const objeto & Stack<objeto>::pop( ){
	//std::cout << "Entrou em pop stack" << endl;
    if( empty() ){
        throw std::underflow_error("  >>> [Stack<T>::pop()] Underflow detected!\n");
    }
    return data[topo--];
}
//
template <class objeto>
const objeto & Stack<objeto>::top() const{
	//std::cout << "Entrou em top stack" << endl;
    if( empty( ) )
        throw std::underflow_error("  >>> [Stack<T>::top()] Underflow detected!\n");
    return data[ topo ];
}
//
template <class objeto>
bool Stack<objeto>::empty( ) const{
	//std::cout << "Entrou em empty stack" << endl;
    return ( topo == -1 );
}
//
template <class objeto>
void Stack<objeto>::clear(){
	std::cout << "Entrou em clear stack" << endl;
    top = -1;
}
//
fila::fila(int _size) : capacity (_size), front(-1), back(-1), data(NULL){
	//std::cout << "Entrou em construtor fila" << endl;
	try {  
		data = new char[ capacity];  
	}
    catch ( std::bad_alloc& e ) {
        std::cerr << "  >>> [Fila<T>::Fila()] Dynamic memory allocation failed!\n";
        throw e;
    }
}

char fila::rm_fila(){
	//std::cout << "Entrou em rm_fila fila" << endl;
	if( empty( ) )
        throw std::underflow_error("  >>> [Fila<T>::rm_fila()] Underflow detected!\n");

	front++;
	return data[front % capacity];
}

void fila::in_fila( const char & _x ){
	//std::cout << "Entrou em in_fila fila" << endl;
    if( (back - capacity) == front ){
        DoubleArray( data, capacity );
    }
    
    back++;
    data[back % capacity] = _x;
}

const char & fila::file_front() const{
	//std::cout << "Entrou em file_front fila" << endl;
	return data[ front ];
}

bool fila::empty() const{
	//std::cout << "Entrou em empty fila" << endl;
	return ( back == front);
}

void fila::clear( ){
	//std::cout << "Entrou em clear fila" << endl;
    back = -1;
    front = -1;
}

template <typename Key, typename Info>
int LLSeq<Key, Info>::buscaPriv (Key _x) const{
	//std::cout << "Entrou em buscaPriv LLSeq" << endl;
	int i = 0;
	while ((i < this->comprimento) && (this->mpt_Data[i].id != _x)){
		i++;
	}
	
	return i;
}

template <typename Key, typename Info>
LLSeq<Key, Info>::LLSeq ( int _MaxSz ) : comprimento(0), capacity(_MaxSz), mpt_Data( NULL ){
	//std::cout << "Entrou em construtor LLSeq" << endl;
    mpt_Data = new NoLLSeq[ capacity ];
}

template <typename Key, typename Info>
LLSeq<Key, Info>::~LLSeq(){
	//std::cout << "Entrou em destrutor LLSeq" << endl;
	delete [] mpt_Data;
}



template <typename Key, typename Info>
bool LLSeq<Key, Info>::buscaPub (Key _x, Info &_valor_rm) const{
	//std::cout << "Entrou em buscaPub LLSeq" << endl;
	if (buscaPriv(_x) < comprimento){
		_valor_rm = mpt_Data[buscaPriv(_x)].info;
		return true;
	}else{
		return false;
	}
}

template <typename Key, typename Info>
bool LLSeq<Key, Info>::rm_llseq (Key _x, Info &_valRemovido){
	//std::cout << "Entrou em bool rm_llseq" << endl;
	int procurado, i;
	if (comprimento != 0){
		procurado = buscaAux(_x);
		if (procurado != comprimento && mpt_Data[procurado].id == _x){
			_valRemovido = mpt_Data[procurado].info;
			for (i=procurado; i<(comprimento-1); i++){
				mpt_Data[i] = mpt_Data[i+1];
				comprimento--;
			}
		}else{
			return false;
		}
	}else{
		throw std::underflow_error("  >>> Underflow detectado!\n");
	}
	
	return true;
}

template <typename Key, typename Info>
bool LLSeq<Key, Info>::in_llseq (Key _novaId, Info _novaInfo){
	//std::cout << "Entrou em bool in_llseq" << endl;
	if (this->comprimento < this->capacity){
		if (buscaPriv(_novaId) == this->comprimento){
			this->mpt_Data[this->comprimento].id = _novaId;
			this->mpt_Data[this->comprimento].info = _novaInfo;
			this->comprimento++;
		}else{
			return false;
		}
	}else{
		throw std::overflow_error("  >>> Overflow detectado!\n");
	}
	
	return true;
}

template <typename Key, typename Info>
void LLSeq<Key, Info>::print () const{
	//std::cout << "Entrou em void print" << endl;
	cout << "Lista: { ";
	for (int i=0; i<this->comprimento; i++){
		cout << mpt_Data[i].info << " ";
	}
	cout << "}" << endl;
}

bool procedencia (char operando1, char operando2){
	//std::cout << "Entrou em bool procedencia" << endl;
	if (operando1 == '@'){
		return true;
	}else if ((operando1 == '^') && (operando2 != '@')){
		return true;
	}else if ((operando1 == '*' || operando1 == '/' || operando1 == '%') && (operando2 != '@' && operando2 != '^')){
		return true;
	}else if ((operando1 == '+' || operando1 == '-') && (operando2 == '-' || operando2 == '+')){
		return true;
	}else{
		return false;
	}
}

fila infixa_posfixa(fila filainfixa){
	//std::cout << "Entrou em infixa_posfixa" << endl;
	
	fila fila_posfixa;
	Stack<char> operandos;
	char symb, topSym;
	
	while (!filainfixa.empty()){
		
		symb = filainfixa.rm_fila();
		
		if (isdigit(symb)){
			fila_posfixa.in_fila(symb);
		}else{
			while (!operandos.empty() && procedencia(operandos.top(), symb)){
				topSym = operandos.top();
				if (procedencia(topSym, symb)){
					topSym = operandos.pop();
					fila_posfixa.in_fila(topSym);
				}
			}
			operandos.push(symb);
		}
	}
	
	while(!operandos.empty()){
		topSym = operandos.pop();
		fila_posfixa.in_fila(topSym);
	}
	
	return fila_posfixa;
}

int avaliacao_posfixa(fila fila_posfixa){
	//std::cout << "Entrou em avaliação_posfixa" << endl;
	char symb;
	int operador1, operador2;
	Stack<int> PilhadeInteiros;
	int resultado;
	
	while (!fila_posfixa.empty()){
		symb = fila_posfixa.rm_fila();
		if (isdigit(symb)){
			PilhadeInteiros.push(symb - '0');
		}else{
			operador2 = PilhadeInteiros.pop();
			operador1 = PilhadeInteiros.pop();
			if (symb == '+'){
				resultado = operador1 + operador2;
			}else if (symb == '-'){
				resultado = operador1 - operador2;
			}else if (symb == '*'){
				resultado = operador1 * operador2;
			}else if (symb == '/'){
				resultado = operador1/operador2;
			}else if (symb == '%'){
				resultado = operador1 % operador2;
			}else if (symb == '^'){
				resultado = 1;
				if (operador2 == 0){
					resultado = 1;
				}else if (operador2 == 1){
					resultado = operador1;
				}else{
					for (int i=0; i<operador2; i++){
						resultado = resultado * operador1;
					}
				}
			}
			PilhadeInteiros.push(resultado);
		}
	}
	
	if (!PilhadeInteiros.empty()){
		resultado = PilhadeInteiros.pop();
	}else{
		resultado = 0;
	}
	
	return resultado;

}