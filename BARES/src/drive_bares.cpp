#include <fstream>
#include <sstream>
#include <iostream>
#include "bares.h"
using namespace std;

int main (int argc, char *argv[] ){

	if (argc < 2){
		cout << "Sintaxe incorreta!" << endl;
		cout << "Use: saida <input_file>" << endl;
		return 0;
	}
	
	ifstream dados_bares;
	string linha;
	istringstream iss;
	int i = 0, j = 0;
	char aux;
	
	dados_bares.open(argv[1], std::ios::in);
	 
	if (!dados_bares.is_open()){
        cerr << "Falha na abertura do arquivo [" << argv[1] << "]! Encerrando o programa...\n";
        return 0;
	}
	
	while (!dados_bares.eof()){
	
		getline(dados_bares, linha);
		if (!dados_bares.eof()){
			iss.str(linha);
		}
		
		LLSeq<int, char> tokens;
		
		while (linha[i]){
			if (!isspace(linha[i])){
				tokens.in_llseq(j, linha[i]);
				j++;
			}
			i++;
		}
		tokens.print();

		fila simbolos, simbolos_out;
		int resultado;
		
		for (i=0; i<j; i++){
			tokens.buscaPub(i, aux);
			if (isdigit(aux)){
				simbolos.in_fila(aux);
			}else if (aux == '+' || aux == '-' || aux == '*' || 
					  aux == '/' || aux == '^' || aux == '%'){
				simbolos.in_fila(aux);
			}
		}
		
		simbolos_out = infixa_posfixa(simbolos);
	
		resultado = avaliacao_posfixa(simbolos_out);
		cout << "O resultado da expressão é: " << resultado << endl << endl;
		
		i = 0;
		j = 0;
	}
	
    dados_bares.close();
	
	return 0;
}


